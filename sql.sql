CREATE TABLE users(
id int auto_increment primary key,
name varchar(255),
email varchar(255),
password varchar(255)

);
CREATE TABLE categories(
id int auto_increment primary key,
name varchar(255)
);
CREATE TABLE items(
id int auto_increment primary key,
name varchar(255),
description varchar(255),
price int,
stock int,
category_id int,
 foreign key(category_id) references categories(id)
);

insert into users(name,email,password) values('John Doe','john@doe.com','john123');
insert into users(name,email,password) values('Jane Doe','jane@doe.com','jenita123');

insert into categories(name) values ('gadget');
insert into categories(name) values ('cloth');
insert into categories(name) values ('men');
insert into categories(name) values ('women');
insert into categories(name) values ('branded');

insert into items(name, description, price, stock, category_id) values ('Sumsang b50', 'hape keren dari merek sumsang',	4000000,100,1);
insert into items(name, description, price, stock, category_id) values ('Uniklooh', 'baju keren dari brand ternama	',	500000,50,2);
insert into items(name, description, price, stock, category_id) values ('IMHO Watch', 'jam tangan anak yang jujur banget	',	2000000,10,1);

select id,name,email from users;
select * from items where name like 'uniklo%';
select * from items where price > 1000000;

select * from items inner join categories on items.category_id = categories.id;

update items set price = 2500000 where id = 1;
update items set price = 2500000 where id = 'Sumsang b50';